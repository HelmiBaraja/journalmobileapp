package helmi.branded.me.medicaljournal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import helmi.branded.me.medicaljournal.Controllers.API;
import helmi.branded.me.medicaljournal.Models.Journal;

public class JournalActivity extends AppCompatActivity {

    private ListView listView;
    private String accessToken;

    private API mAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_journal);

        //get accessToken from Login page.
        accessToken = getIntent().getStringExtra(this.getString(R.string.accessToken));

        mAuthTask = new API(this);
        mAuthTask.subscription(accessToken, new API.APIListener() {
            @Override
            public void apiResponseSuccess(Object output) {

              populateListView((ArrayList<Journal>) output);

            }

            @Override
            public void apiResponseFailed(String output) {

            }
        });

        //click listener of listview

    }

    private void populateListView(final ArrayList<Journal> arrayList)
    {
        //cast back the object

        //initialize custom list layout
        CustomList customList = new CustomList(this, arrayList);

        //set up listview
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(customList);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(JournalActivity.this
                        ,JournalReaderActivity.class);
                intent.putExtra(getString(R.string.journalId), arrayList.get(i).journalId);
                intent.putExtra(getString(R.string.accessToken), accessToken);
                startActivity(intent);
            }
        });
    }


}
