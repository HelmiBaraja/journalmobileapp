package helmi.branded.me.medicaljournal.Models;

/**
 * Created by HelmiHasan on 27/08/2016.
 */
public class User {


    public User(String accessToken, String userId, String username, int userType) {
        this.accessToken = accessToken;
        this.userId = userId;
        this.username = username;
        this.userType = userType;
    }

    public String accessToken;
    public String userId;
    public String username;
    public int userType;
}
