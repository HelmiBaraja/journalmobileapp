package helmi.branded.me.medicaljournal;

/**
 * Created by HelmiHasan on 26/08/2016.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import helmi.branded.me.medicaljournal.Models.Journal;

public class CustomList extends ArrayAdapter<Journal> {
    private ArrayList<Journal>arrayList;

    private Activity context;


    public CustomList(Activity context, ArrayList<Journal> arrayList) {
        super(context, R.layout.journal_row, arrayList);

        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.journal_row, null, true);
        TextView journalTitle = (TextView) listViewItem.findViewById(R.id.journalTitle);
        TextView publisherName = (TextView) listViewItem.findViewById(R.id.publisherName);

        journalTitle.setText(arrayList.get(position).journalTitle);
        publisherName.setText(arrayList.get(position).publisherName);
        return  listViewItem;
    }
}