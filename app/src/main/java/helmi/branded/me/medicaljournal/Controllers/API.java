package helmi.branded.me.medicaljournal.Controllers;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import helmi.branded.me.medicaljournal.HBURLConnection.HBUrlConnection;
import helmi.branded.me.medicaljournal.Models.Journal;
import helmi.branded.me.medicaljournal.Models.User;
import helmi.branded.me.medicaljournal.R;

/**
 * Created by HelmiHasan on 27/08/2016.
 */
public class API implements HBUrlConnection.HBUrlListener {

    private final String Base_URL =  "http://10.0.2.2:8000";

    private final String LoginAPI = Base_URL+"/api/login";
    private final String SubscriptionsAPI = Base_URL+"/api/subscriptions";
    private final String DocumentAPI = Base_URL+"/api/document";


    /* Create interface to be used callback method to caller class */
    public interface APIListener {
        void apiResponseSuccess(Object output);

        void apiResponseFailed(String output);
    }

    public APIListener listener = null;
    HBUrlConnection urlConnection = null;
    private Context context = null;
    private String auditNo = null;
    public API(final Context context) {
        this.context = context;
        this.urlConnection = new HBUrlConnection();

    }

    public void login(String userName, String password, APIListener apiListener) {
        final HashMap<String, String> map = new HashMap<String, String>();
        auditNo = LoginAPI;

        map.put(context.getString(R.string.userName), userName);
        map.put(context.getString(R.string.password), password);

        //this to return response back to this method.
        urlConnection.listener= this;

        //this to return back to the caller.
        listener = apiListener;

        urlConnection.sendPost(LoginAPI, urlConnection.convertToParams(map));
    }

    public void subscription(String accessToken, APIListener apiListener) {

        auditNo = SubscriptionsAPI;


        //this to return response back to this method.
        urlConnection.listener= this;

        //this to return back to the caller.
        listener = apiListener;

        urlConnection.accessToken = accessToken;
        urlConnection.sendGet(SubscriptionsAPI);
    }

    public void document(String journalId, String accessToken, APIListener apiListener)
    {
        auditNo = DocumentAPI;


        //this to return response back to this method.
        urlConnection.listener= this;

        //this to return back to the caller.
        listener = apiListener;

        urlConnection.accessToken = accessToken;
        urlConnection.sendGet(DocumentAPI+"/"+journalId);
    }
    /**
     * Implementation callback of HBURL COnnection
     */
    @Override
    public void requestDidFinish(String output) {
        try {
            switch (auditNo) {
                case LoginAPI:

                    processLoginAPI(output);

                    break;
                case SubscriptionsAPI:
                    processSubscriptionsAPI(output);
                    break;
                case DocumentAPI:
                    processDocumentAPI(output);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestDidFail(String output) {

    }

    private void processLoginAPI(String output) throws JSONException {
        Log.d("output", output);

        JSONObject json = new JSONObject(output);

        //get accessToken from output
        String accessToken = json.getString(context.getString(R.string.accessToken));
        String userId = json.getString(context.getString(R.string.userId));
        String username = json.getString(context.getString(R.string.username));
        int userType = Integer.parseInt(json.getString(context.getString(R.string.userType)));

        User user = new User(accessToken,userId,username,userType);

        listener.apiResponseSuccess(user);
    }

    private void processSubscriptionsAPI(String output) throws JSONException {
        JSONArray json = new JSONArray(output);

        List<Journal> arrayList = new ArrayList<>();
        for (int i =0 ; i<json.length(); i++)
        {
            JSONObject object = json.getJSONObject(i);
            String journalId = object.getString(context.getString(R.string.journalId));
            String journalTitle = object.getString(context.getString(R.string.journalTitle));
            String uploadedOn = object.getString(context.getString(R.string.uploadedOn));
            String publisherId = object.getString(context.getString(R.string.publisherId));
            String publisherName = object.getString(context.getString(R.string.publisherName));

            Journal journal = new Journal(journalId,journalTitle,uploadedOn,publisherId,publisherName);

            arrayList.add(journal);
        }

        listener.apiResponseSuccess(arrayList);
    }

    private void processDocumentAPI(String output)
    {
        listener.apiResponseSuccess(output);
    }


}
