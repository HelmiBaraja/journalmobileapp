package helmi.branded.me.medicaljournal.Models;

/**
 * Created by HelmiHasan on 26/08/2016.
 */
public class Journal {

    public String journalId;
    public String journalTitle;
    public String uploadedOn;
    public String publisherId;
    public String publisherName;

    public Journal(String journalId, String journalTitle, String uploadedOn, String publisherId, String publisherName) {
        this.journalId = journalId;
        this.journalTitle = journalTitle;
        this.uploadedOn = uploadedOn;
        this.publisherId = publisherId;
        this.publisherName = publisherName;
    }
}
