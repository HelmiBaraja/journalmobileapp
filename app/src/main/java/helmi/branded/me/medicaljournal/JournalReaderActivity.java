package helmi.branded.me.medicaljournal;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import helmi.branded.me.medicaljournal.Controllers.API;
import helmi.branded.me.medicaljournal.HBURLConnection.Downloader;

public class JournalReaderActivity extends AppCompatActivity {

    PDFView pdfView ;
    private API mAuthTask = null;
    private String accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //disable screenshots from pdf reader.
        disableScreenshots();

        setContentView(R.layout.activity_journal_reader);

//        pdfView = (PDFView) findViewById(R.id.pdfView);
//        pdfView.fromAsset("sample.pdf")
//                .load();

        accessToken = getIntent().getStringExtra(this.getString(R.string.accessToken));
        String journalId = getIntent().getStringExtra(this.getString(R.string.journalId));

        downloadPdf();
//        mAuthTask = new API(this);
//        mAuthTask.document(journalId, accessToken, new API.APIListener() {
//            @Override
//            public void apiResponseSuccess(Object output) {
//
//                String pdf = (String) output;
//                byte[] pdfAsBytes = Base64.decode(pdf.getBytes(), 0);
//
//                File filePath = new File(Environment.getExternalStorageDirectory() + "/filename.pdf");
//                FileOutputStream os = null;
//                try {
//                    os = new FileOutputStream(filePath, true);
//                    os.write(pdfAsBytes);
//                    os.flush();
//                    os.close();
//
//                    pdfView = (PDFView) findViewById(R.id.pdfView);
//                 pdfView.fromFile(filePath)
//                .load();
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//            @Override
//            public void apiResponseFailed(String output) {
//
//            }
//        });
    }

    private void disableScreenshots()
    {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

    }

    public void downloadPdf()
    {
        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "pdf");
        folder.mkdir();
        File file = new File(folder, "Read.pdf");
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        Downloader.DownloadFile("http://www.nmu.ac.in/ejournals/aspx/courselist.pdf", file);

        viewPdf();
    }

    public void viewPdf()
    {
        File file = new File(Environment.getExternalStorageDirectory()+"/pdf/Read.pdf");
        PackageManager packageManager = getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(file);

        pdfView = (PDFView) findViewById(R.id.pdfView);
        pdfView.fromFile(file)
                .load();
//        intent.setDataAndType(uri, "application/pdf");
//        startActivity(intent);
    }

}
